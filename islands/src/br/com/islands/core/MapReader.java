package br.com.islands.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import br.com.islands.util.Constants;

/**
 *
 */
public class MapReader {

	/**
	 * Run application and prints results
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		MapScanner mapScanner = new MapScanner();
		String fileName =  Constants.MAP_TEXT_FILE_PATH;
		readMap(mapScanner, fileName);
		
		System.out.println("Area: " + mapScanner.getArea());
		System.out.println("Perimeter: " + mapScanner.getPerimeter());
		System.out.println("Islands: " + mapScanner.getIslands());
	}

	/**
	 * Read map.txt 
	 * 
	 * @param mapScanner new instance of file scanner
	 * @param fileName path and file name containing map
	 */
	public static void readMap(MapScanner mapScanner, String fileName) {

		File file = new File(fileName);
		
		if (file.exists()) {
			try {
				FileReader fileReader = new FileReader(file);
				BufferedReader bufferedReader = new BufferedReader(fileReader);
				String currentLine = "";
				
				while ((currentLine = bufferedReader.readLine()) != null) {
					mapScanner.Scan(currentLine);
				}
				bufferedReader.close();
			} catch (FileNotFoundException e) {
				System.out.println("could not find file");
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
