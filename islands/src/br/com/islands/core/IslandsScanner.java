package br.com.islands.core;

import java.util.ArrayList;
import java.util.List;

import br.com.islands.util.Constants;

public class IslandsScanner {
	
	/**
	 * Calculate how many islands exists on map using Lists for each new island
	 * 
	 * @return islands
	 */
	public static int calculateIslands(List<Integer> islandsElements) {
		
		int islands;
		
		List<List<Integer>> islandsIntList = new ArrayList<List<Integer>>();
		
		List<Integer> leftElementList ;
		List<Integer> upperElementList ;
		
		for (int currentElement : islandsElements) {
			
			leftElementList = new ArrayList<Integer>();
			upperElementList = new ArrayList<Integer>();
			
			if (islandsElements.contains(currentElement - 1)) {
				leftElementList = searchList(currentElement -1, islandsIntList);
				leftElementList.add(currentElement);
			}
			
			if (islandsElements.contains(currentElement - Constants.LINE_MULTIPLIER)) {
				upperElementList = searchList(currentElement - Constants.LINE_MULTIPLIER, islandsIntList);
				if (leftElementList.isEmpty() || upperElementList.equals(leftElementList)) {
					upperElementList.add(currentElement);
				} else {
					leftElementList.addAll(upperElementList);
					islandsIntList.remove(upperElementList);
				}
			}
			if (leftElementList.isEmpty() && upperElementList.isEmpty()) {
				createNewList(islandsIntList, currentElement);
			}
		}
		islands = islandsIntList.size();
		
		return islands;
	}

	/**
	 * Creates new island and insert it to islandsList
	 * 
	 */
	private static void createNewList(List<List<Integer>> islandsIntList, int currentElement) {
		List<Integer> newList = new ArrayList<Integer>();
		newList.add(currentElement);
		islandsIntList.add(newList);
	}
	
	/**
	 * Search element in islandsList
	 * 
	 * @return list containing element
	 */
	private static List<Integer> searchList(int currentElement, List<List<Integer>> islandsIntList) {
		List<Integer> foundList = new ArrayList<Integer>();
		for (List<Integer> currentList : islandsIntList) {
			if (currentList.contains(Integer.valueOf(currentElement))) {
				foundList = currentList;
				break;
			}
		}
		return foundList;
	}
}
