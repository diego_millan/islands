package br.com.islands.core;

import java.util.ArrayList;
import java.util.List;

import br.com.islands.util.Constants;


public class MapScanner {
	
	private int lineCount;
	private int area;

	private List<List<Integer>> perimeterMap;
	private List<Integer> islandsElements;
	
	public MapScanner(){
		perimeterMap = new ArrayList<List<Integer>>(); 
		islandsElements = new ArrayList<Integer>();
	}
	
	/**
	 * Read values from file to update area and lists
	 * 
	 * @param currentLine
	 */
	public void Scan(String currentLine) {
		
		int currentValue;
		perimeterMap.add(new ArrayList<Integer>());

		String[] values = currentLine.split(" ");
		
		for (int i = 0; i < values.length; ++i) {
			currentValue = Integer.valueOf(values[i]);
			incrementArea(currentValue);
			addMapValue(currentValue, lineCount);
			incrementIslandMap(currentValue, i);
		}
		
		System.out.println("line " + lineCount + " - " + perimeterMap.get(lineCount));
		
		lineCount++;

	}

	/**
	 * Add current (line * LINE_MULTIPLIER + column) to list 
	 * line multiplier is a way to make unique integers to search lists with performance  
	 * 
	 * @param currentValue
	 * @param column
	 */
	private void incrementIslandMap(int currentValue, int column) {
		if (currentValue > Constants.SEA_LEVEL) {
			islandsElements.add(lineCount * Constants.LINE_MULTIPLIER + column);
		}
	}

	/**
	 * Add currentValue to perimeter map
	 * 
	 * @param currentValue current element
	 * @param lineCount current line of file
	 */
	private void addMapValue(int currentValue, int lineCount) {
		perimeterMap.get(lineCount).add(currentValue);
	}
	
	/**
	 * Check if current value is greater than zero to increment area value.
	 * 
	 * @param currentValue
	 */
	private void incrementArea(int currentValue) {
		if (currentValue > 0) {
			area++;
		}
	}
	
	/**
	 * Calculate perimeter for values > 0 comparing with left and upper element 
	 * 
	 * @return perimeter
	 */
	public int getPerimeter() {
		return PerimeterScanner.calculatePerimeter(perimeterMap);
	}

	/**
	 * Calculate how many islands exists on map using Lists for each new island
	 * 
	 * @return islands
	 */
	public int getIslands() {
		
		return IslandsScanner.calculateIslands(islandsElements);
		
	}

	public int getArea() {
		return area;
	}
	
	public int getLineCount() {
		return lineCount;
	}

	public List<List<Integer>> getPerimeterMap() {
		return perimeterMap;
	}

	public List<Integer> getIslandsElements() {
		return islandsElements;
	}

}