package br.com.islands.core;

import java.util.List;

import br.com.islands.util.Constants;

public class PerimeterScanner extends MapScanner {
	
	private static List<List<Integer>> perimeterMap;
	
	/**
	 * Calculate perimeter for values > 0 comparing with left and upper element 
	 * 
	 * @return perimeter
	 */
	public static int calculatePerimeter(List<List<Integer>> map) {
		
		perimeterMap = map;
		
		int lineSize = perimeterMap.size();
		int perimeter = 0;
		int columnSize = perimeterMap.get(Constants.FIRST_ELEMENT).size();
		
		for (int line = 0; line < lineSize; ++line) {
			for (int column = 0; column < columnSize; ++column) {
				if (currentElementFromMap(line, column) > 0) {
					if (line == 0) {
						perimeter++;
					} else {
						if (isLastLineOrColumnFromMap(lineSize, line)) {
							perimeter++;
						}
						
						if (elementInPreviousLineFromMap(line, column) == 0) {
							perimeter++;
						}
					}
					if (column == 0) {
						perimeter++;
					} else {
						if (isLastLineOrColumnFromMap(columnSize, column)) {
							perimeter++;
						}
						if (elementInPreviousColumnFromMap(line, column) == 0) {
							perimeter++;
						}
					}
					
				} else {
					perimeter = checkPerimeterForZeroValuesFromMap(perimeter, line, column);
				}
			}
		}
		return perimeter;
	}

	/**
	 * Check from map if previous element is greater than zero
	 * 
	 * @param perimeter
	 * @param line
	 * @param column
	 * @return perimeter updated
	 */
	private static int checkPerimeterForZeroValuesFromMap(int perimeter, int line, int column) {
		if (line > 0 && elementInPreviousLineFromMap(line, column) > 0) {
			perimeter++;
		}
		if (column > 0 && elementInPreviousColumnFromMap(line, column) > 0) {
			perimeter++;
		}
		return perimeter;
	}

	/**
	 * 
	 * @param listSize
	 * @param line
	 * @return true if is last line or column
	 */
	private static boolean isLastLineOrColumnFromMap(int listSize, int line) {
		return (line == listSize - 1);
	}

	/**
	 * 
	 * @param line
	 * @param column
	 * @return Element value
	 */
	private static Integer currentElementFromMap(int line, int column) {
		return perimeterMap.get(line).get(column);
	}

	/**
	 * 
	 * @param line
	 * @param column
	 * @return Element value
	 */
	private static Integer elementInPreviousColumnFromMap(int line, int column) {
		return perimeterMap.get(line).get(column - 1);
	}

	/**
	 * Get element in previous line
	 * 
	 * @param line
	 * @param column
	 * @return Element value
	 */
	private static Integer elementInPreviousLineFromMap(int line, int column) {
		return perimeterMap.get(line - 1).get(column);
	}
}
