package br.com.islands.test;

import br.com.islands.core.MapReader;
import br.com.islands.core.MapScanner;

public class TestUtils {

	protected static MapScanner executeScan(String fileName) {
		MapScanner mapScanner = new MapScanner();
		MapReader.readMap(mapScanner, fileName);
		return mapScanner;
	}
}
