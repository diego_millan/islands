package br.com.islands.test;

import org.junit.Assert;
import org.junit.Test;

import br.com.islands.core.MapScanner;
import br.com.islands.util.Constants;

public class PerimeterTest {

	@Test
	public void testPerimeterFirstExample() {
		String fileName = Constants.MAP_TEXT_1_FILE_PATH;
		MapScanner mapScanner = TestUtils.executeScan(fileName);
		Assert.assertSame(44, mapScanner.getPerimeter());
	}

	@Test
	public void testPerimeterSecondeExample() {
		String fileName =  Constants.MAP_TEXT_2_FILE_PATH;
		MapScanner mapScanner = TestUtils.executeScan(fileName);
		Assert.assertSame(12, mapScanner.getPerimeter());
	}
	
	@Test
	public void testPerimeterThirdExample() {
		String fileName =  Constants.MAP_TEXT_3_FILE_PATH;
		MapScanner mapScanner = TestUtils.executeScan(fileName);
		Assert.assertSame(72, mapScanner.getPerimeter());
	}
	
}
