package br.com.islands.test;

import org.junit.Assert;
import org.junit.Test;

import br.com.islands.core.MapScanner;
import br.com.islands.util.Constants;

public class AreaTest {

	@Test
	public void testAreaExample1() {
		String fileName = Constants.MAP_TEXT_1_FILE_PATH;
		MapScanner mapScanner = TestUtils.executeScan(fileName);
		Assert.assertSame(54, mapScanner.getArea());
	}

	@Test
	public void testAreaExample2() {
		String fileName =  Constants.MAP_TEXT_2_FILE_PATH;
		MapScanner mapScanner = TestUtils.executeScan(fileName);
		Assert.assertSame(5, mapScanner.getArea());
	}
	
	@Test
	public void testAreaExample3() {
		String fileName =  Constants.MAP_TEXT_3_FILE_PATH;
		MapScanner mapScanner = TestUtils.executeScan(fileName);
		Assert.assertSame(68, mapScanner.getArea());
	}
}
