package br.com.islands.test;

import org.junit.Assert;
import org.junit.Test;

import br.com.islands.core.MapScanner;
import br.com.islands.util.Constants;

public class IslandsTest {

	@Test
	public void testIslandsFirstExample() {
		String fileName =  Constants.MAP_TEXT_1_FILE_PATH;
		MapScanner mapScanner = TestUtils.executeScan(fileName);
		Assert.assertSame(3, mapScanner.getIslands());
	}

	@Test
	public void testIslandsSecondeExample() {
		String fileName =  Constants.MAP_TEXT_2_FILE_PATH;
		MapScanner mapScanner = TestUtils.executeScan(fileName);
		Assert.assertSame(1,mapScanner.getIslands());
	}
	
	@Test
	public void testIslandsThirdExample() {
		String fileName =  Constants.MAP_TEXT_3_FILE_PATH;
		MapScanner mapScanner = TestUtils.executeScan(fileName);
		Assert.assertSame(4,mapScanner.getIslands());
	}
}
