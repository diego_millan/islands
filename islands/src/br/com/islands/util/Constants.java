package br.com.islands.util;

public class Constants {

	public static final String MAP_TEXT_1_FILE_PATH = "maps//mapTests1.txt";
	
	public static final String MAP_TEXT_2_FILE_PATH = "maps//mapTests2.txt";
	
	public static final String MAP_TEXT_3_FILE_PATH = "maps//mapTests3.txt";

	public static final String MAP_TEXT_FILE_PATH = "maps//map.txt";
	
	public static final int FIRST_ELEMENT = 0;

	public static final int LINE_MULTIPLIER = 100;

	public static final int SEA_LEVEL = 1;

}
