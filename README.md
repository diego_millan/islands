# README #

### What is this repository for? ###

* Projeto classificatório da Samsung.
* Version 1.0

### How do I get set up? ###

* O projeto é escrito em java, contém uma classe main e uma classe mestre para execução de testes
* O projeto depende da instalação da JDK do java para execução do jar por linha de comando
* Para execução dos testes, deve-se executar as classes de teste pela IDE
* Para executar os testes, executar com o JUnit 4 o arquivo br.com.islands.test.TestsSuite.java
* O método Main está localizado na classe br.com.islands.core.MapReader.java 
* A leitura do mapa é feita através do arquivo maps.txt que pode ser encontrado na pasta maps

### Who do I talk to? ###

* Owner: Diego Rodrigo Millan
* diego.yrm@gmail.com