package br.com.islands.core;

import java.util.ArrayList;
import java.util.List;


public class MapScanner {
	
	private static final int LINE_MULTIPLIER = 100;
	private static final int SEA_LEVEL = 1;
	
	private int lineCount;
	private int area;
	private int islands;

	private List<List<Integer>> perimeterMap;
	private List<Integer> islandsElements;
	
	public MapScanner(){
		perimeterMap = new ArrayList<List<Integer>>(); 
		islandsElements = new ArrayList<Integer>();
	}
	
	/**
	 * Read values from file to update area and lists
	 * 
	 * @param currentLine
	 */
	public void Scan(String currentLine) {
		
		int currentValue;
		perimeterMap.add(new ArrayList<Integer>());

		String[] values = currentLine.split(" ");
		
		for (int i = 0; i < values.length; ++i) {
			currentValue = Integer.valueOf(values[i]);
			incrementArea(currentValue);
			addMapValue(currentValue, lineCount);
			incrementIslandMap(currentValue, i);
		}
		
		System.out.println("line " + lineCount + " - " + perimeterMap.get(lineCount));
		
		lineCount++;

	}

	private void incrementIslandMap(int currentValue, int column) {
		if (currentValue > SEA_LEVEL) {
			islandsElements.add(lineCount * LINE_MULTIPLIER + column);
		}
	}

	private void addMapValue(int currentValue, int lineCount) {
		perimeterMap.get(lineCount).add(currentValue);
	}
	
	private void incrementArea(int currentValue) {
		if (currentValue > 0) {
			area++;
		}
	}
	
	/**
	 * Calculate perimeter for values > 0 comparing with left and upper element 
	 * 
	 * @return perimeter
	 */
	public int getPerimeter() {

		int lineSize = perimeterMap.size();
		int perimeter = 0;
		
		for (int i = 0; i < lineSize; ++i) {
			int columnSize = perimeterMap.get(i).size();
			for (int n = 0; n < columnSize; ++n) {
				if (perimeterMap.get(i).get(n) > 0) {
					if (i == 0) {
						perimeter++;
					} else {
						if (i == lineSize - 1) {
							perimeter++;
						}
						
						if (perimeterMap.get(i - 1).get(n) == 0) {
							perimeter++;
						}
					}
					if (n == 0) {
						perimeter++;
					} else {
						if (n == columnSize - 1) {
							perimeter++;
						}
						if (perimeterMap.get(i).get(n - 1) == 0) {
							perimeter++;
						}
					}
					
				} else {
					if (i > 0 && perimeterMap.get(i - 1).get(n) > 0) {
						perimeter++;
					}
					if (n > 0 && perimeterMap.get(i).get(n - 1) > 0) {
						perimeter++;
					}
				}
			}
		}
		return perimeter;
	}

	/**
	 * Calculate how many islands exists on map using Lists for each new island
	 * 
	 * @return islands
	 */
	public int calculateIslands() {
		
		List<List<Integer>> islandsIntList = new ArrayList<List<Integer>>();
		
		List<Integer> leftElementList ;
		List<Integer> upperElementList ;
		
		for (int currentElement : islandsElements) {
			
			leftElementList = new ArrayList<Integer>();
			upperElementList = new ArrayList<Integer>();
			
			if (islandsElements.contains(currentElement - 1)) {
				leftElementList = searchList(currentElement -1, islandsIntList);
				leftElementList.add(currentElement);
			}
			
			if (islandsElements.contains(currentElement - LINE_MULTIPLIER)) {
				upperElementList = searchList(currentElement - LINE_MULTIPLIER, islandsIntList);
				if (leftElementList.isEmpty() || upperElementList.equals(leftElementList)) {
					upperElementList.add(currentElement);
				} else {
					leftElementList.addAll(upperElementList);
					islandsIntList.remove(upperElementList);
				}
			}
			if (leftElementList.isEmpty() && upperElementList.isEmpty()) {
				createNewList(islandsIntList, currentElement);
			}
		}
		islands = islandsIntList.size();
		
		return islands;
	}

	/**
	 * Creates new island and insert it to islandsList
	 * 
	 */
	private void createNewList(List<List<Integer>> islandsIntList, int currentElement) {
		List<Integer> newList = new ArrayList<Integer>();
		newList.add(currentElement);
		islandsIntList.add(newList);
	}
	
	/**
	 * Search element in islandsList
	 * 
	 * @return list containing element
	 */
	private List<Integer> searchList(int currentElement, List<List<Integer>> islandsIntList) {
		List<Integer> foundList = new ArrayList<Integer>();
		for (List<Integer> currentList : islandsIntList) {
			if (currentList.contains(Integer.valueOf(currentElement))) {
				foundList = currentList;
				break;
			}
		}
		return foundList;
	}

	public int getArea() {
		return area;
	}

	public int getIslands() {
		return islands;
	}

}
